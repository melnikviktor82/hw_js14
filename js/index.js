"use strict";

const btnChangeTheme = document.createElement("button");
btnChangeTheme.type = "button";
btnChangeTheme.innerText = "Change Theme";

const contacts = document.querySelector(".contacts");
contacts.append(btnChangeTheme);

const logoText = document.querySelector(".logo-text");
const mail = document.querySelector(".mail");
const tel = document.querySelector(".tel");

const colorArr = [logoText, mail, tel];

const tourTextArr = document.querySelectorAll(".tour-text");
const hiddenBlockArr = document.querySelectorAll(".hidden-block");

let color = "rgb(0, 0, 255)";
color = localStorage.getItem("theme");

btnChangeTheme.addEventListener("click", () => {
  if (btnChangeTheme.style.backgroundColor === "rgb(242, 101, 34)") {
    color = "rgb(0, 0, 255)";
    ChangeTheme(color);
    localStorage.setItem("theme", color);
  } else {
    color = "rgb(242, 101, 34)";
    ChangeTheme(color);
    localStorage.setItem("theme", color);
  }
});

let ChangeTheme = function (color) {
  colorArr.forEach((elem) => {
    elem.addEventListener("mouseover", (event) => {
      event.target.style.color = color;
    });
    elem.addEventListener("mouseout", (event) => {
      event.target.style.color = "white";
    });
  });

  tourTextArr.forEach((elem) => {
    elem.style.color = color;
  });

  hiddenBlockArr.forEach((elem) => {
    elem.style.backgroundColor = color;
  });

  btnChangeTheme.style.backgroundColor = color;
};

ChangeTheme(color);

